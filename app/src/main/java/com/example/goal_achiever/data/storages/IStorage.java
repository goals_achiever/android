package com.example.goal_achiever.data.storages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public interface IStorage {
    ArrayList<HashMap<String, String>> get(String modelName);
    HashMap<String, String> getById(String modelName, UUID id);
    HashMap<String, String> create(String modelName, HashMap<String, String> params);
    HashMap<String, String> update(String modelName, UUID id, HashMap<String, String> params);
    boolean destroy(String modelName, UUID id);
}
