package com.example.goal_achiever.data.storages;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class LocalStorage implements IStorage {
    private static final int DATABASE_VERSION = 6;
    private static final String DATABASE_NAME = "goals_achiever_db";
    private static final String DB_FIELD_TEXT = "text";
    private static final String DB_FIELD_INT = "integer";
    private static final HashMap<String, String> REQUIRED_FIELDS = new HashMap<>();
    static {
        REQUIRED_FIELDS.put("uuid", "text");
        REQUIRED_FIELDS.put("created_at", "integer");
        REQUIRED_FIELDS.put("updated_at", "integer");
    }
    private static final String REQUIRED_FIELDS_SQL = "uuid text primary key, created_at integer, updated_at integer";

    private static final HashMap<String, HashMap<String, String>> TABLES = new HashMap<>();
    static {
        HashMap<String, String> taskFields = new HashMap<>();
        taskFields.put("name", DB_FIELD_TEXT);
        TABLES.put("Task", taskFields);
    }

    private static LocalStorage instance;
    private static SQLiteDatabase database;

    public static LocalStorage initialize(Context context) {
        if(instance != null)
            return instance;

        database = performDatabase(context);
        instance = new LocalStorage();
        return instance;
    }

    public static LocalStorage getInstance() {
        return instance;
    }

    @Override
    public ArrayList<HashMap<String, String>> get(String modelName) {
        Cursor cursor = database.query(modelName, null, null, null, null, null, "created_at desc");
        return getRecords(modelName, cursor);
    }

    @Override
    public HashMap<String, String> getById(String modelName, UUID id) {
        String[] selectionArgs = {id.toString()};
        Cursor cursor = database.query(modelName, null, "uuid =?", selectionArgs, null, null, "created_at desc", "1");
        ArrayList<HashMap<String, String>> records = getRecords(modelName, cursor);
        if(records.size() == 0)
            return null;

        return records.get(0);
    }

    @Override
    public HashMap<String, String> create(String modelName, HashMap<String, String> params) {
        ContentValues contentValues = hashToContentValues(params);
        database.insertOrThrow(modelName, null, contentValues);
        return params;
    }

    @Override
    public HashMap<String, String> update(String modelName, UUID id, HashMap<String, String> params) {
        ContentValues contentValues = hashToContentValues(params);
        database.update(modelName, contentValues, "uuid = '" + id + "'", null);
        return params;
    }

    @Override
    public boolean destroy(String modelName, UUID id) {
        database.delete(modelName, "uuid = '" + id + "'", null);
        return true;
    }

    private static SQLiteDatabase performDatabase(Context context) {
        SQLiteOpenHelper sqliteOpenHelper = new SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {
            @Override
            public void onCreate(SQLiteDatabase db) {
                for(Map.Entry<String, HashMap<String, String>> table : TABLES.entrySet()) {
                    String tableName = table.getKey();
                    HashMap<String, String> columns = table.getValue();
                    String fieldsQuery = REQUIRED_FIELDS_SQL;
                    if(columns.size() > 0)
                        fieldsQuery += ", ";
                    for(Map.Entry<String, String> column : columns.entrySet()) {
                        String fieldName = column.getKey();
                        String fieldType = column.getValue();
                        fieldsQuery += fieldName + " " + fieldType + ", ";
                    }
                    db.execSQL("create table " + tableName + "(" + fieldsQuery.substring(0, fieldsQuery.length() - 2) + ")");
                }
            }

            @Override
            public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
                if(newVersion <= oldVersion)
                    return;

                for(Map.Entry<String, HashMap<String, String>> table : TABLES.entrySet()) {
                    String tableName = table.getKey();
                    db.execSQL("drop table if exists " + tableName);
                }
                onCreate(db);
            }
        };
        return sqliteOpenHelper.getWritableDatabase();
    }

    private ArrayList<HashMap<String, String>> getRecords(String modelName, Cursor cursor) {
        HashMap<String, String> fields = TABLES.get(modelName);
        fields.putAll(REQUIRED_FIELDS);
        ArrayList<HashMap<String, String>> records = new ArrayList<>();
        if(cursor.moveToFirst()) {
            do {
                HashMap<String, String> record = new HashMap<>();
                for(Map.Entry<String, String> field : fields.entrySet()) {
                    String fieldType = field.getValue();
                    String fieldName = field.getKey();
                    String fieldValue = null;
                    int fieldValueIndex = cursor.getColumnIndex(fieldName);
                    if(fieldType == DB_FIELD_INT)
                        fieldValue = String.valueOf(cursor.getInt(fieldValueIndex));
                    else if(fieldType == DB_FIELD_TEXT)
                        fieldValue = cursor.getString(fieldValueIndex);
                    record.put(fieldName, fieldValue);
                }
                records.add(record);
            } while(cursor.moveToNext());
        }
        return records;
    }

    private ContentValues hashToContentValues(HashMap<String, String> params) {
        ContentValues contentValues = new ContentValues();
        for(Map.Entry<String, String> param : params.entrySet()) {
            String paramName = param.getKey();
            String paramValue = param.getValue();
            contentValues.put(paramName, paramValue);
        }
        return contentValues;
    }
}
