package com.example.goal_achiever.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.goal_achiever.R;
import com.example.goal_achiever.data.storages.LocalStorage;
import com.example.goal_achiever.models.Task;

import java.util.ArrayList;
import java.util.UUID;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        LocalStorage.initialize(this);

//        Task task = new Task(this, "My new task");
//        task.save();

        Task task_new = new Task("My new task 100");
        task_new.save();
        task_new.name = "Updated task 100";
        task_new.save();
        Task task = Task.getById(task_new.uuid);

        TextView idDisplay = findViewById(R.id.id_display);
        idDisplay.setText(task.uuid.toString());

        TextView createdAtDisplay = findViewById(R.id.created_at_display);
        createdAtDisplay.setText(task.createdAt.toString());

        TextView updatedAtDisplay = findViewById(R.id.updated_at_display);
        updatedAtDisplay.setText(task.updatedAt.toString());

        TextView nameDisplay = findViewById(R.id.name_display);
        nameDisplay.setText(task.name);
    }
}
