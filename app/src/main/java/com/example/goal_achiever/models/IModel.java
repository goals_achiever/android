package com.example.goal_achiever.models;

public interface IModel {
    Base save();
    boolean destroy();
}
