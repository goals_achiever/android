package com.example.goal_achiever.models;

import com.example.goal_achiever.data.storages.LocalStorage;
import com.example.goal_achiever.data.storages.IStorage;

import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public abstract class Base {
    public UUID uuid;
    public Date createdAt;
    public Date updatedAt;
    protected IStorage localStorage;

    public Base() {
        localStorage = LocalStorage.getInstance();
    };

    public Base(UUID uuid_, Date createdAt_, Date updatedAt_) {
        uuid = uuid_;
        createdAt = createdAt_;
        updatedAt = updatedAt_;
    }

    public Base(HashMap<String, String> params) {
        uuid = UUID.fromString(params.get("uuid"));
        createdAt = convertStrToDate(params.get("created_at"));
        updatedAt = convertStrToDate(params.get("updated_at"));
    }


    public Base save() {
        if(createdAt == null) {
            createdAt = new Date();
        }
        updatedAt = new Date();
        return this;
    }

    protected boolean destroyInStorage(String modelName) {
        boolean result = localStorage.destroy(modelName, uuid);
        return result;
    }

    protected Base saveToStorage(String modelName) {
        HashMap<String, String> attributes = toHashMap();
        if(uuid == null) {
            uuid = UUID.randomUUID();
            attributes.put("uuid", uuid.toString());
            localStorage.create(modelName, attributes);
        } else {
            localStorage.update(modelName, uuid, attributes);
        }
        return this;
    }

    protected HashMap<String, String> toHashMap() {
        HashMap<String, String> attributes = new HashMap<>();
        attributes.put("created_at", convertDateToString(createdAt));
        attributes.put("updated_at", convertDateToString(updatedAt));
        return attributes;
    }

    protected Date convertStrToDate(String dateStr) {
        return new Date(Integer.parseInt(dateStr) * 1000L);
    }

    protected String convertDateToString(Date date) {
        Integer timeInt = (int) (date.getTime() / 1000);
        return timeInt.toString();
    }
}
