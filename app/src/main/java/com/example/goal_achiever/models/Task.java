package com.example.goal_achiever.models;

import android.content.Context;

import com.example.goal_achiever.data.storages.IStorage;
import com.example.goal_achiever.data.storages.LocalStorage;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.UUID;

public class Task extends Base implements IModel {
    private static final String modelName = "Task";

    public String name;

    public Task(UUID uuid_, Date createdAt_, Date updatedAt_, String name_) {
        super(uuid_, createdAt_, updatedAt_);
        name = name_;
    }

    public Task(HashMap<String, String> params) {
        super(params);
        name = params.get("name");
    }

    public Task(String name_) {
        super();
        name = name_;
    }

    public static ArrayList<Task> get() {
        ArrayList<HashMap<String, String>> records = LocalStorage.getInstance().get(modelName);
        ArrayList<Task> objects = new ArrayList<>();
        for(HashMap<String, String> record : records) {
            objects.add(new Task(record));
        }
        return objects;
    }

    public static Task getById(UUID id) {
        HashMap<String, String> record = LocalStorage.getInstance().getById(modelName, id);
        if(record == null)
            return null;

        return new Task(record);
    }

    @Override
    public Base save() {
        super.save();
        saveToStorage(modelName);
        return this;
    }

    @Override
    public boolean destroy() {
        boolean result = destroyInStorage(modelName);
        return result;
    }

    protected HashMap<String, String> toHashMap() {
        HashMap<String, String> attributes = super.toHashMap();
        attributes.put("name", name);
        return attributes;
    }
}
